import { Component } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-detall',
  templateUrl: './detall.page.html',
  styleUrls: ['./detall.page.scss'],
})
export class DetallPage {
  moto: any

  constructor(private route: ActivatedRoute, private router: Router) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.moto = this.router.getCurrentNavigation().extras.state.parametros;  
        //console.log(this.moto)
      }
    })
  }

    borrar(id){
      const url = "http://natalia-mas-7e3.alwaysdata.net/miapi/delete?id="+id;
        //const url = "http://motos.puigverd.org/moto/" + id;
        fetch(url, {
          "method": "DELETE"
        })
        .then(response => {
            this.router.navigateByUrl('/home');
          });
      }
    
  

}