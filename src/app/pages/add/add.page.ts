import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage {
  image;
  marca : string;
  modelo : string;
  year : string;
  precio : string;
  
  moto

  constructor( private router: Router) { }

  async add(){
    this.image = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    console.log(this.image)
    //this.image = "http://motos-b60.kxcdn.com/sites/default/files/ducati-panigale-v4-2018-2.jpg"

    this.moto = {
      "marca":this.marca,
      "modelo":this.modelo,
      "year":this.year,
      "foto":this.image,
      "precio":this.precio
    }
    console.log(JSON.stringify(this.moto))
    

    const url = "http://natalia-mas-7e3.alwaysdata.net/miapi/moto?";
        fetch(url, {
           headers: {
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(this.moto)
        })

        this.router.navigateByUrl('/home');

        }
  
  

}
