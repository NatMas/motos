import { Component } from '@angular/core';
import {NavigationExtras,Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  motos
  element = []

  constructor(private router: Router) {
    this.getJson()
  }

  ionViewDidEnter(){
    this.getJson()

  }

  async getJson(){
    const respuesta = await fetch("http://natalia-mas-7e3.alwaysdata.net/miapi/motos");
    //const respuesta = await fetch("http://motos.puigverd.org/motos");
    this.motos= await respuesta.json();
  }

  add(){
    this.router.navigate(['add']);

  }
  vistadetall(moto){
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: moto,
      }
    };
    this.router.navigate(['detall'],navigationExtras);

  }

  async getJsonFiltre(filtre){
    const respuesta = await fetch("http://natalia-mas-7e3.alwaysdata.net/miapi/motosFilt?marca="+filtre);
    //const respuesta = await fetch("http://motos.puigverd.org/motos?marca="+filtre);
    this.motos= await respuesta.json();
  }


}
